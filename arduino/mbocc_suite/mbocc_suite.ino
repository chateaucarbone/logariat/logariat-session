
/* 
  Logariat.
  Mbocc suite :
   - Heart sensor > Led Strip > Serial
   - Button > Blink Led Strip
 */

#include <Bounce2.h>

#define LED_PIN D1
#define HEART_PIN D2
#define BUTTON1_PIN D3
#define BUTTON2_PIN D4

Bounce btn1 = Bounce(); 
Bounce btn2 = Bounce(); 

// State detection
int v0, v1, v2;
int v0_last = 0;
int v1_last = 0;
int v2_last = 0;

// Without delay
unsigned long currentMillis;
unsigned long previousMillis = 0;
const long led_interval = 1000;
int led_tempo [] = { 1000, 200, 500, 300 };
int id = 0;
int ledState = LOW;

// States
boolean isLedHeart = false;
boolean isLedBlink = false;

void setup() {

  pinMode(HEART_PIN, INPUT);

  pinMode(BUTTON1_PIN, INPUT_PULLUP);
  btn1.attach(BUTTON1_PIN);
  btn1.interval(5); // ms
  
  pinMode(BUTTON2_PIN, INPUT_PULLUP);
  btn2.attach(BUTTON2_PIN);
  btn2.interval(5);

  pinMode(LED_PIN,OUTPUT);

  Serial.begin(57600);
}

void loop() {
  btn1.update();
  btn2.update();
  currentMillis = millis();
  
  // Sensors
  v0 = digitalRead(HEART_PIN);
  v1 = btn1.read();
  v2 = btn2.read();

  // Button 1
  if (v1 != v1_last) {
     if ( v1 == LOW ) { // on
       isLedHeart = false;
       isLedBlink = !isLedBlink;
     } else {
       ledState = LOW;
     }
  }

  // Button 2
  if (v2 != v2_last) {
     if ( v2 == LOW ) { // on
      isLedBlink = false;
      isLedHeart = !isLedHeart;
     } else {
      ledState = LOW;
     }
  }

  // Heart
  if (v0 != v0_last) {
   Serial.print(v0);
  }

  if (isLedBlink) {
    led_blink();
  } else if (isLedHeart) {
    heart_led();
  } 

  v0_last = v0;
  v1_last = v1;
  v2_last = v2;
}



void heart_led() {
  if (v0 == HIGH) {
    ledState = HIGH;
    Serial.print(255);
  } else {
    ledState = LOW;
    Serial.print(0);
  }
  digitalWrite(LED_PIN, ledState);
}

void led_blink() {
  if (currentMillis - previousMillis >= led_tempo[id]) {
    //Serial.println(id);
    previousMillis = currentMillis;
    if (ledState == LOW) {
      ledState = HIGH;
    } else {
      ledState = LOW;
    }
    
    digitalWrite(LED_PIN, ledState);
    if (ledState == HIGH) {
      id = ( id + 1 ) % 4;
    }
  }
}
