
// First click (once) > display webcam / login
let step2 = () => {
  document.getElementById('zone2').style.display = 'block'
  document.getElementById('cam1').style.display = 'none'
  document.getElementById('login').style.display = 'block'
  document.removeEventListener('click', step2)
  console.log('Step2')
}
document.addEventListener('click', step2)

window.onload = () => {
  console.log('Load')
  setTimeout( () => {
    console.log('IPCAM SRC')
    document.getElementById('ipcam').src = 'http://192.168.43.1:8080/video'
  }, 5000)
}

//////////////////// Web sockets
const socket = io();

socket.on('sensors', () => {
    console.log('Sensors')
    openWebcam()
    document.getElementById('graphs').style.display = 'block'
    document.getElementById('zone3').style.display = 'block'
    let sketch1 = new p5(drawGraphs, 'graphs')
    let sketch2 = new p5(drawShapes, 'zone3')
});

socket.on('questionnaire', () => {
    document.getElementById('login').scrolling = "yes";
    document.getElementById('login').src = "questionnaire.html";
});

// socket.on('terminal', () => {
//     document.getElementById('login').scrolling = "no";
//     document.getElementById('login').src = "terminal.html";
// });

socket.on('reglementations', () => {
    document.getElementById('login').scrolling = "no";
    document.getElementById('login').src = "reglementations.html";
    //socket.emit('bigscreen', 1)
});



/////////////////// webcam

const openWebcam = () => {
  const video = document.getElementById("webcam");
  const videoConstraints = { video: { width: 1280, height: 720, frameRate: 20 }}; // webcam
  if (navigator.mediaDevices.getUserMedia) {
    navigator.mediaDevices.getUserMedia(videoConstraints)
    .then( (stream) => {
      video.srcObject = stream;
      document.getElementById('cam2').style.display = 'block'
    })
    .catch( (error) => {
      console.log("Something went wrong!", error);
    });
  }
}
