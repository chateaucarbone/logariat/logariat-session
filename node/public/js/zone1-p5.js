
let drawGraphs = (sketch) => {
    let w = 640;
    let h = 1080;
    let g1 = [];
    let g2 = [];
    let socket;
    let g1_data = 0;
    let g2_data = 0;
    let gHeight = 400;
    let gSize = w;
    let yOffset = 540;
    let heart_data = [1, 0.5, 0.25, 0.12, 0.06, 0.0]
    let heart_data_index = 0;

    sketch.setup = () => {
      console.log('Sketch 1');
      socket = io.connect();

      sketch.createCanvas(w, h);
      sketch.noFill();
      sketch.stroke(255);
      sketch.strokeWeight(2);
      sketch.frameRate(20);

      for (let i = 0; i < gSize ; i++) {
        g1.push(0);
        g2.push(0);
      }

      socket.on('keyboard', (data) => {
          g1_data = data;
          //console.log('keyboard', data);
      })

      socket.on('heart', (data) => {
          //console.log('heart', data);
          if (data === 1) {
            heart_data_index = 0;
            for (let i = 1; i <= heart_data.length; i++) {
              let coef = (heart_data.length / i ) / heart_data.length;
              //console.log(coef);
              heart_data[i-1] = sketch.random(coef-0.05, coef+0.05)
            }
          }
          g2_data = data;
      });
  }

  sketch.draw = () => {
    sketch.clear();

    // Push data
    if ( (g2_data != 0)  && (heart_data_index < heart_data.length) )
    {
      g2_data = heart_data[heart_data_index] * 250;
      //heart_data_index = (heart_data_index + 1 ) % heart_data.length;
      heart_data_index++;
    }

    g1.push(g1_data);
    g2.push(g2_data);

    // Affichage graphs
    sketch.noFill();
    sketch.stroke(255);

    // Graph1
    sketch.beginShape();
    for (let i = 0; i < g1.length ; i++)
    {
      let y = sketch.map (g1[i], 0, 1, gHeight, 0);
      sketch.vertex(i, g1[i] + yOffset);
    }
    sketch.endShape();

    // Graph2
    sketch.beginShape();
    for (let i = 0; i < g2.length ; i++)
    {
      //let y = sketch.map (g2[i], 0, 255, 0, gHeight);
      sketch.vertex(i, -g2[i] + yOffset + 125);
    }
    sketch.endShape();

    if (g1.length > gSize) g1.splice(0,1);
    if (g2.length > gSize) g2.splice(0,1);

    // Affichage chiffres
    sketch.fill(255);
    sketch.noStroke();
    sketch.text(g1_data, sketch.width-30, yOffset);
    sketch.text(g2_data, 30,  yOffset);
  }
}
