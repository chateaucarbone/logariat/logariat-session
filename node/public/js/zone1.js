// WebCam
const video = document.getElementById("camera");
const videoConstraints = { video: { width: 1280, height: 720, frameRate: 20 }}; // webcam
// 15, 20, 25, 30
// resolution in hd >   1080*640; in 940*720 > 
// codec mjpeg/h264
// transform css : scale (optimisé), height:100% / width:auto
// <img width="" height="">
// <video width="" height="">


//frameRate: { ideal: 10, max: 15 }

if (navigator.mediaDevices.getUserMedia) {
  navigator.mediaDevices.getUserMedia(videoConstraints)
  .then( (stream) => {
    video.srcObject = stream;
  })
  .catch( (error) => {
    console.log("Something went wrong!", error);
  });
}
