Terminal.applyAddon(fit);
const term = new Terminal({
  fontSize: 20,
  cursorBlink: true,
  fontFamily: 'Ubuntu Mono, courier-new, courier, monospace',
  'theme': { background: '#000000' }
});
//term.resize();
term.open(document.getElementById('zone2'));
term.fit();  // terminal's size = #terminal
//term.write('Welcome to \x1B[1;3;31mxterm.js\x1B[0m $ ')
term.write('$ ');

let command = '';

term.on('key', (key, ev) => {
  //console.log(key)
  if (key.charCodeAt(0) == 13) {
    //console.log('Command:', command)
    if (command === 'cam1') {
        document.getElementById('camera').style.display = 'inline'
        document.getElementById('ipCamWrapper').style.display = 'none'
    } else if (command === 'cam2' ) {
      document.getElementById('camera').style.display = 'none'
      document.getElementById('ipCamWrapper').style.display = 'inline'
    }
    command = ''
  } else {
    command += key
  }

  socket.emit('shell', key); // envoie web socket shell serveur (index.js)
})

/*
// keys > client > server > OSC
const keyCodes =
  {0:["That key has no keycode",!1],3:["break",!1],8:["backspace / delete",!1],9:["tab",!1],12:["clear",!1],13:["enter",!1],16:["shift",!1],17:["ctrl",!1],18:["alt",!1],19:["pause/break",!1],20:["caps lock",!1],21:["hangul",!1],25:["hanja",!1],27:["escape",!1],28:["conversion",!1],29:["non-conversion",!1],32:["spacebar",!1],33:["page up",!1],34:["page down",!1],35:["end",!1],36:["home",!1],37:["left arrow",!1],38:["up arrow",!1],39:["right arrow",!1],40:["down arrow",!1],41:["select",!1],42:["print",!1],43:["execute",!1],44:["Print Screen",!1],45:["insert",!1],46:["delete",!1],47:["help",!1],48:["0",!1],49:["1",!1],50:["2",!1],51:["3",!1],52:["4",!1],53:["5",!1],54:["6",!1],55:["7",!1],56:["8",!1],57:["9",!1],58:[":",!1],59:["semicolon (firefox), equals",!1],60:["<",!1],61:["equals (firefox)",!1],63:["ß",!1],64:["@ (firefox)",!1],65:["a",!1],66:["b",!1],67:["c",!1],68:["d",!1],69:["e",!1],70:["f",!1],71:["g",!1],72:["h",!1],73:["i",!1],74:["j",!1],75:["k",!1],76:["l",!1],77:["m",!1],78:["n",!1],79:["o",!1],80:["p",!1],81:["q",!1],82:["r",!1],83:["s",!1],84:["t",!1],85:["u",!1],86:["v",!1],87:["w",!1],88:["x",!1],89:["y",!1],90:["z",!1],91:["Windows Key / Left ⌘ / Chromebook Search key",!1],92:["right window key",!1],93:["Windows Menu / Right ⌘",!1],95:["sleep",!1],96:["numpad 0",!1],97:["numpad 1",!1],98:["numpad 2",!1],99:["numpad 3",!1],100:["numpad 4",!1],101:["numpad 5",!1],102:["numpad 6",!1],103:["numpad 7",!1],104:["numpad 8",!1],105:["numpad 9",!1],106:["multiply",!1],107:["add",!1],108:["numpad period (firefox)",!1],109:["subtract",!1],110:["decimal point",!1],111:["divide",!1],112:["f1",!1],113:["f2",!1],114:["f3",!1],115:["f4",!1],116:["f5",!1],117:["f6",!1],118:["f7",!1],119:["f8",!1],120:["f9",!1],121:["f10",!1],122:["f11",!1],123:["f12",!1],124:["f13",!1],125:["f14",!1],126:["f15",!1],127:["f16",!1],128:["f17",!1],129:["f18",!1],130:["f19",!1],131:["f20",!1],132:["f21",!1],133:["f22",!1],134:["f23",!1],135:["f24",!1],144:["num lock",!1],145:["scroll lock",!1],160:["^",!1],161:["!",!1],162:["؛ (arabic semicolon)",!1],163:["#",!1],164:["$",!1],165:["ù",!1],166:["page backward",!1],167:["page forward",!1],168:["refresh",!1],169:["closing paren (AZERTY)",!1],170:["*",!1],171:["~ + * key",!1],172:["home key",!1],173:["minus (firefox), mute/unmute",!1],174:["decrease volume level",!1],175:["increase volume level",!1],176:["next",!1],177:["previous",!1],178:["stop",!1],179:["play/pause",!1],180:["e-mail",!1],181:["mute/unmute (firefox)",!1],182:["decrease volume level (firefox)",!1],183:["increase volume level (firefox)",!1],186:["semi-colon / ñ",!1],187:["equal sign",!1],188:["comma",!1],189:["dash",!1],190:["period",!1],191:["forward slash / ç",!1],192:["grave accent / ñ / æ / ö",!1],193:["?, / or °",!1],194:["numpad period (chrome)",!1],219:["open bracket",!1],220:["back slash",!1],221:["close bracket / å",!1],222:["single quote / ø / ä",!1],223:["`",!1],224:["left or right ⌘ key (firefox)",!1],225:["altgr",!1],226:["< /git >, left back slash",!1],230:["GNOME Compose Key",!1],231:["ç",!1],233:["XF86Forward",!1],234:["XF86Back",!1],235:["non-conversion",!1],240:["alphanumeric",!1],242:["hiragana/katakana",!1],243:["half-width/full-width",!1],244:["kanji",!1],251:"unlock trackpad (Chrome/Edge)",255:["toggle touchpad",!1]};

$('.xterm-helper-textarea').keydown(function() {
    if (keyCodes[event.keyCode][1]==false) {
      keyCodes[event.keyCode][1]=true
       socket.emit('keydown', event.keyCode)
    }
});

$('.xterm-helper-textarea').keyup(function() {
    if (keyCodes[event.keyCode][1]==true) {
      keyCodes[event.keyCode][1]=false
      socket.emit('keyup', event.keyCode)
    }
});
*/
