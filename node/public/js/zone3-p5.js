var drawShapes = (sketch) => {
  let pg;
  let w = 640;
  let h = 1080;
  let x = 0;
  let xoffset = 0;
  let y = 20;
  let charSize = 30;
  let charPoints = [ '/', '\\', '-', '#', '//', '|', '_' ];
  let charPointsIndex = 0;
  let speed = 10;
  let randomSpeed = 500;
  let socket;
  let points = [];
  let points2 = [];
  let points3 = [];
  
  let points4 = [
    [ 1280, 0 ],
    [ 1920, 0 ],
    [ 1920, 1080 ],
   	[ 1280, 1080 ],
    [ 1280, 900 ],
    [ 1280, 800 ],
    [ 1280, 700 ],
    [ 1280, 600 ],
    [ 1280, 500 ],
    [ 1280, 400 ],
    [ 1280, 300 ],
    [ 1280, 100 ]
   ]
    
  let pointsMaxSize = 8;
  let countKey = 0;
  let renderShapes = false;
  let randomKeyModulo = 50;
  
  let bigScreen = false;

  sketch.setup = () => {
    console.log('Sketch 3');
    socket = io.connect();

    // Settings shapes
    sketch.createCanvas(w, h);
    sketch.stroke(255,0,0,20);
    sketch.noFill();
    sketch.strokeWeight(2);
    sketch.textStyle(sketch.BOLD);

    // Settings dessin accumulation
    pg = sketch.createGraphics(w, h);
    pg.noStroke();
    pg.fill(20,20,20,8);
    pg.textSize(charSize);
    pg.background(255);

    // Websocket
    socket.on('keyboard', (data) => {
      //console.log('key', data);
        x = ( x + 8) % sketch.width;
        if (renderShapes)
        {
          countKey++;
          if ( (countKey % randomKeyModulo ) === 0 ) {
            charPointsIndex = sketch.int(sketch.random(0, charPoints.length));
            randomKeyModulo = sketch.int(sketch.random(30, 70));
          }
        }
    });

    socket.on('shapes', (data) => {
      data === 0 ? renderShapes = false : renderShapes = true;
    });
    
    /*
    socket.on('bigscreen', (data) => {
    	console.log('bigscreen',data )
      //data === 0 ? bigScreen = false : bigScreen = true;
      
      bigScreen = true;
      sketch.resizeCanvas(1920,1080)
      renderShapes = false
    });*/
  }

  sketch.draw = () => {
    sketch.clear();

    // Graphics
    if ( (sketch.frameCount % speed) === 0 ) y++;
    if ( y > h ) y = 0;

    //pg.fill(100, 100, (sketch.frameCount /5) % 255, 3);
    //pg.ellipse(x + xoffset, y, charSize, charSize);
    pg.text(charPoints[charPointsIndex], x + xoffset, y);
    sketch.image(pg, 0, 0);
    
    /*
    if (bigScreen) {
    	
    	 // Shape 1
        sketch.strokeWeight(5);
        sketch.stroke(255,255,255,10);
        sketch.fill(0);
        sketch.noStroke();
        sketch.beginShape();
        for (let i = 4; i < (points4.length - 4 ); i++)
        {
          sketch.vertex(points4[i][0] + sketch.random(-3, 3), points4[i][1] + sketch.random(-3, 3));
        }
        sketch.endShape(sketch.CLOSE);	
    
    }*/

    // formes
    if (renderShapes) {
      // Add vertices
      if ( (sketch.frameCount % randomSpeed) == 0 )
      {
        if ( points.length < pointsMaxSize)
        {
          points.push([x,y]);
        } else {
          points2.push([x,y]);
        }

        randomSpeed = sketch.int(sketch.random(400,1000));
      }


      if ( (sketch.frameCount % ( randomSpeed - sketch.int(sketch.random(-100,200))) == 0 )  && ( points2.length >= pointsMaxSize) )
      {
        points3.push([x,y]);
      }

        // Shape 1
        sketch.strokeWeight(5);
        sketch.stroke(0,0,0);
        sketch.noFill();
        sketch.beginShape();
        for (let i = 0; i < points.length ; i++)
        {
          sketch.vertex(points[i][0] + sketch.random(1), points[i][1] + sketch.random(1));
        }
        sketch.endShape();

        // Shape 2
        sketch.strokeWeight(2);
        sketch.stroke(0,0,0);
        sketch.fill(0,0,0,20)
        sketch.beginShape();
        for (let i = 0; i < points2.length ; i++)
        {
          sketch.vertex(points2[i][0] + sketch.random(-2,2), points2[i][1] + sketch.random(-2,2));
        }
        sketch.endShape();


        // Shape 3
        sketch.strokeWeight(sketch.random(5,10));
        sketch.stroke(0,20,0,60);
        sketch.fill(0,0,0,20);
        sketch.beginShape();
        for (let i = 0; i < points3.length ; i++)
        {
          sketch.vertex(points3[i][0] + sketch.random(2), points3[i][1] + sketch.random(2));
        }
        sketch.endShape(sketch.CLOSE);
  	  } // if renderShapes

    } // fin draw
}
