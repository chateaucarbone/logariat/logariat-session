
// Three.js
  var scene = new THREE.Scene();
  var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );
  var renderer = new THREE.WebGLRenderer( { alpha: true } );
  renderer.setClearColor( 0x000000, 0 );
  var container = document.getElementById('zone3');
  renderer.setSize($(container).width(), $(container).height(),false);
  container.appendChild(renderer.domElement);
  camera.aspect = $(container).width() / $(container).height();
  camera.updateProjectionMatrix();
  scene.add( new THREE.HemisphereLight( 0x606060, 0x404040 ) );
  var light = new THREE.DirectionalLight( 0xffffff );
  light.position.set( 1, 1, 1 ).normalize();
  scene.add( light );
  var geometry = new THREE.BoxGeometry( 1, 1, 1 );
  cube = new THREE.Mesh( geometry, new THREE.MeshLambertMaterial( { color: Math.random() * 0xffffff } ) );
  scene.add( cube );
  camera.position.z = 5;
  var animate = function () {
    requestAnimationFrame( animate );
    cube.rotation.x += 0.1;
    cube.rotation.y += 0.01;
    renderer.render( scene, camera );
  };
  animate();
