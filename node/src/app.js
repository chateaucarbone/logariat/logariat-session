// Libs
const express = require('express')
const http = require('http')
const path = require('path')

// Settings
const port = 3000;
const app = express()
app.set('port', port)
app.use(express.static(path.join(__dirname, '../public'))) // Setup absolute static directory to serve

// HTTP Server
const server = app.listen(port, () => {
  console.log('Web app running at: http://localhost:'+port+'/')
})


//////////////////////// IO
const io = require('socket.io').listen(server)

io.on('connection', socket => {
  // demo
	//socket.emit('request', /* … */) // emit an event to the socket
	//io.emit('broadcast', /* … */) // emit an event to all connected sockets
	//socket.on('reply', () => { /* … */ }) // listen to the event

	socket.on('fromXterm', (data) => { // xterm > pty
    //console.log('xterm > pty', data);
		ptyProcess.write(data)
	})

  socket.on('ouvrirReglementations', () => {
		io.emit('reglementations')
	})
	
	socket.on('ouvrirBigScreen', () => {
		io.emit('bigscreen')
	})

  // socket.on('ouvrirTerminal', () => {
  //   io.emit('terminal')
  // })

  // socket.on('ouvrirWebcam', () => {
	// 	socket.emit('webcam')
	// })
})

//////////////////////// shell
const pty = require('node-pty')

const ptyProcess = pty.spawn('bash', [], {
  name: 'xterm-color',
  cwd: path.join(__dirname, '../../home'),
  env: process.env
})

ptyProcess.on('data', (data) => {
  //process.stdout.write(data)
  //console.log('pty > xterm', data);
  io.emit('fromPty', data) // pty > xterm
})

process.on('exit', () => ptyProcess.kill());

/////////////////////////// get

app.get('/questionnaire', (req, res) => {
    io.emit('questionnaire');
})

app.get('/sensors', (req, res) => {
    io.emit('sensors');
})

app.get('/shapes', (req, res) => {
    io.emit('shapes', 1);
})

/*app.get('/pd', (req, res) => {
    pd.create();
})*/

//////////////////////// SERIAL
const SerialPort = require('serialport')
const serial = new SerialPort('/dev/ttyUSB0', function (err) { // /dev/ttyUSB0 ou /dev/ttyACM0
  if (err) {
    return console.log('Error: ', err.message)
  }
})

serial.on('data', function (inputData) {
	const json = JSON.stringify(inputData);
	const parsed = JSON.parse(json);
	//console.log('heart_node', parsed.data[0]);
	//const buffer = inputData.toString()
	parsed.data[0] === 255 ? io.emit('heart', 1) : io.emit('heart', 0)

	oscServer.send({
        address: "/heart/raw",
        args: [
            {
                type: "i",
                value: parsed.data[0]
            }
        ]
    }, "127.0.0.1", 9100);

	//io.emit('heart', parsed.data[0])
})


//////////////////////// OSC
const osc = require('osc');

const oscServer = new osc.UDPPort({
    localAddress: '127.0.0.1',
		localPort: 9000
});

oscServer.on('ready', function () {
    console.log("Listening for OSC over UDP. 9000");
});

oscServer.on('message', function (oscMessage) {
		if (oscMessage.address === '/key'){
      //console.log('python key osc');
      io.emit('keyboard', oscMessage.args[0])
    }
		//else if (oscMessage.address === '/heart/raw') console.log('HEART:', oscMessage.args[0])
		//else if (oscMessage.address === '/render/shapes') io.emit('renderShapes', oscMessage.args[0])
});

oscServer.open();


const oscServer2 = new osc.UDPPort({
    localAddress: '127.0.0.1',
		localPort: 9001
});

oscServer2.on('ready', function () {
    console.log("Listening for OSC over UDP. 9001");
});

oscServer2.on('message', function (oscMessage) {
	if (oscMessage.address === '/shapes') io.emit('shapes', oscMessage.args[0])
});

oscServer2.open();
