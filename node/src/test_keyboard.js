// xev | node src/keyboard.js 
/*
const xevEmitter = require('xev-emitter')(process.stdin)

xevEmitter.on('KeyPress', (key) => {
    console.log(key, 'was pressed')
})

xevEmitter.on('KeyRelease', (key) => {
    console.log(key, 'was released')
})*/


/*
    const readline = require('readline');
    readline.emitKeypressEvents(process.stdin);
    process.stdin.setRawMode(true);
    process.stdin.on('keypress', (str, key) => {
      if (key.ctrl && key.name === 'c') {
        process.exit();
      } else {
        console.log(`You pressed the "${str}" key`);
        console.log();
        console.log(key);
        console.log();
      }
    });
    
    console.log('Press any key...');
   
    */
    
var Keyboard = require('node-keylogger');

var k = new Keyboard('event1'); // 'event0' is the file corresponding to my keyboard in /dev/input/
k.on('keyup', console.log);
k.on('keydown', console.log);
k.on('keypress', console.log);
k.on('error', console.error);