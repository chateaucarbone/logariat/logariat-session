
//////////////////////// osc

/*
// Libs
const dgram = require('dgram')
const oscClient = dgram.createSocket('udp4')
const oscServer = dgram.createSocket('udp4')
const osc = require('a2r-osc')

// Settings
const host = 'localhost'
const oscPort = 9001

oscServer.on('listening', () => {
  const address = '127.0.0.1';
  console.log('Listening OSC messages at: ${address.port}');
})

oscServer.bind(9000);

// Server
oscServer.on('message', (msg, rinfo) => {
  const arr = msg.toString('utf8').split(',')[0].split('/')

  console.log('OSC received: '+arr[0]+' '+arr[1])

  // router tout message OSC envoyés par pd vers clients node.
  // ex: number 33
  //io.emit(''+arr[0], ''+arr[1].replace(/[^a-zA-Z0-9]/g, ''))

  // example
  if (arr[0]=='message') {
  	// faire un truc...
  }
})

*/


/*
// keyboard > osc
const keyUp = (key) => {
  //return new osc.Message('/key' + key, 'i', 1).toBuffer()
  return new osc.Message('/key', 's', key+' 1').toBuffer()
}
const keyDown = (key) => {
  //return new osc.Message('/key' + key, 'i', 0).toBuffer()
  return new osc.Message('/key', 's', key+' 0').toBuffer()
}
const keyShell = (key) => {
  return new osc.Message('/key', 's', key).toBuffer()
}
const send = (key) => {
  oscClient.send(message, oscPort, host, (err, bytes) => {
    if(err) throw new Error(err)
  })
}
*/

// Close
/*process.on('SIGINT', () => {
  oscClient.close()
})*/


const osc = require('osc');

const oscServer = new osc.UDPPort({
    localAddress: '127.0.0.1',
		localPort: 9100
});

oscServer.on('ready', function () {
    console.log("Listening for OSC over UDP.");
    
    oscServer.send({
        address: "/heart/raw",
        args: [
            {
                type: "i",
                value: 122
            }
        ]
    }, "127.0.0.1", 9101);
});

oscServer.on('message', function (oscMessage) {
		if (oscMessage.address === '/key') console.log('KEY:', oscMessage.args[0])
		else if (oscMessage.address === '/heart/raw') console.log('HEART:', oscMessage.args[0])
});

oscServer.open();