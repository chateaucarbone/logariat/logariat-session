const osc = require('osc');

const oscServer2 = new osc.UDPPort({
    localAddress: '127.0.0.1',
		localPort: 9001
});

oscServer2.on('ready', function () {
    console.log("Listening for OSC over UDP. 9001");
});

oscServer2.on('message', function (oscMessage) {
 console.log(oscMessage);
	//if (oscMessage.address === '/render/shapes') io.emit('renderShapes', oscMessage.args[0])
});

oscServer2.open();