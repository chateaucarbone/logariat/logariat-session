const Port = require('port');


//pd -nomidi -open $PD_MAIN -path "$DIR/../pd/plugins/kiosk-plugin/" -path "$DIR/../pd/plugins/completion-plugin/" 2>&1 &

const pd = new Port({
	'read': 8005,
	'write': 8006,
	'encoding': 'ascii',
	'basepath': __dirname,
	'flags': {
		//'noprefs': false,
		//'stderr': false,
		//'send': 'pd dsp 1',
		//'open': './port.pd'
		'nomidi' : true,
		'path': '../../pd/plugins/kiosk-plugin/',
		'path': '../../pd/plugins/completion-plugin/',
		'open': '../../pd/main.pd'
		}	
})
.on('connect', function(){
	console.log('Port connected');
	this.write('a message connected to pd;\n');
})
.on('data', function(data){
	console.log('From [netsend]', data);
	this.write('un message de Node vers Pd;\n');
})

/*
pd.on('stderr', function(buffer){
	console.log('on stderr');
	console.log(buffer.toString());
})*/

//pd.create();

/*
setTimeout(() => {
	pd.write('Nouveau message de la part de Node;\n');
	}, 2000);
	
	

*/

setTimeout(() => {
	pd.create();
	}, 4000);


