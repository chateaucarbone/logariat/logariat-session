const SerialPort = require('serialport')
const port = new SerialPort('/dev/ttyUSB0', function (err) {
  if (err) {
    return console.log('Error: ', err.message)
  }
})

port.write('main screen turn on', function(err) {
  if (err) {
    return console.log('Error on write: ', err.message)
  }
  console.log('message written')
})


port.on('data', function (inputData) {
	const json = JSON.stringify(inputData);
	const parsed = JSON.parse(json);
	//const buffer = inputData.toString()
	if (parsed.data[0] === 255) console.log('♥')
})
