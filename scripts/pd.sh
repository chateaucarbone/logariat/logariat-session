#! /bin/bash
# Open patch

killall pd 2>&1

# Get absolute path of this script
DIR="$(cd "$(dirname "$0")" && pwd)"

# Patch names
PD_MAIN="$DIR/../pd/main.pd"
PD_IDLE="$DIR/../pd/_idle.pd"
PD_BLANK="$DIR/../pd/_blank.pd"

# Clear main patch
rm $PD_MAIN
cp $PD_BLANK $PD_MAIN

# Start pd main
# Prblème avec -path "$DIR/../pd/plugins/completion-plugin/"
pd -nomidi -open $PD_MAIN  -path "$DIR/../pd/plugins/kiosk-plugin/" -send "pd dsp 1"  &
