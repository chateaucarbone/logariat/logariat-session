#! /bin/bash
# Start Logariat script session

# Clear history
history -c && history -w

# --------------- VARIABLES ------------------

# Colors
NORMAL='\e[0m'
GREEN='\e[92m'
BOLD='\e[1m'
RED='\e[31m'
GRAY='\e[37m'
BLACK_B='\e[40m'
GREEN_B='\e[42m'
WHITE_B='\e[107;30m'
RED_B='\e[41;97m'

# Get absolute path of this script
DIR="$(cd "$(dirname "$0")" && pwd)"

# Scripts list
# SCRIPT_CAPTEURS="$DIR/capteurs.sh"
SCRIPT_PD="$DIR/pd.sh"

ID="#BF0EI"
LOGIN="mbocc"

# --------------- FONCTIONS ------------------
function debut_session(){
	clear
	echo -e $NORMAL
	echo -e "##########################"
	echo -e "#### Logarien.Session ####"
	echo -e "##########################"
	DATE=$(date +%A" "%d.%m.%y" "%R)
	JOUR=$(date +%A" "%d.%m.%y)
	#echo -e "$DATE"
	echo -e "samedi 08.06.19 20:05"
	echo
	#echo -e -n "$WHITE_B\bIdentifiant:$NORMAL "
	#read LOGIN
	#echo -e -n "$WHITE_B\bMot de passe:$NORMAL "
	#read -s PASS
	#echo
	#echo -e "### Pour l'aide tapez help ###"
	#echo
	echo -e "Bonjour $BOLD$LOGIN$NORMAL, vous êtes dorénavant connecté."
	echo -e "Vous aller réaliser de magnifiques tâches cette nuit."
	#echo -e "Nous vous souhaitons un agréable travail."
	echo
	echo -e -n "$GRAY\bVérification en cours "
}

function run_sensors(){
	# Erreur Capteur
	echo
	echo -e "$RED_B[Erreur : 303]$NORMAL Veuillez activer les capteurs."
	echo -e "$GRAY\bArticle 102.38.1$NORMAL de loi cadre relative à votre santé."
	echo -e -n "Code d'activation : "
	read CAPTEUR
	echo
	curl http://localhost:3000/sensors 2>&1 &
	echo -e -n "Activation en cours "
	#$SCRIPT_CAPTEURS &
}

function affiche_resultats(){
	echo
	echo -e "Résultats : diagnostics $ID"
	echo
	echo -e "$WHITE_B PRIORITÉ | %COEUR | %DOIGTS | TEMPS   | PROCESSUS           $NORMAL"
	echo -e " 20       | 12,0   | 14,6    | 0:01:09 | virtualworker/3 "
	echo -e " 20       | 14,2   | 15,1    | 0:03:52 | net-assist"
	echo -e " rt       | 12,5   | 7,2     | 0:02:11 | chien-surveillant/2"
	echo -e " rt       | 12,9   | 12,2    | 0:01:20 | sensations-imprevues"
	echo -e " 0        | 11,4   | 14,8    | 0:00:43 | taches-assist"
}

# ---- MESSAGES
function message_1(){
	echo
	echo -e "$WHITE_B\bDe:$NORMAL Administrateur"
	echo
	echo -e "$WHITE_B\bSujet:$NORMAL $RED_B[Urgent] Rapport de sécurité $ID$NORMAL"
	echo
	echo -e "$WHITE_B\bDate:$NORMAL $JOUR 19:55:31"
	echo
	echo -e "$WHITE_B\bCorps:$NORMAL Cette nuit, le service de sécurité a signalé un"
	echo -e "incident de classe A lié à une intrusion dans la base des"
	echo -e "utilisateurs  $BOLD\blogWorld$NORMAL. Vous êtes en charge de diagnostiquer"
	echo -e "l'incident et de procéder si besoin au nettoyage de la base"
	echo -e "utilisateurs. Conformément à votre statut de collaborateur,"
	echo -e "l'acte doit être co-signé par votre chef de service."
	echo
}

function message_2(){
	echo
	echo -e "$WHITE_B\bDe:$NORMAL Responsable du bohneur"
	echo
	echo -e "$WHITE_B\bSujet:$NORMAL Rapport d'évaluation"
	echo
	echo -e "$WHITE_B\bDate:$NORMAL $JOUR 12:20:01"
	echo
	echo -e "$WHITE_B\bCorps:$NORMAL Le rapport d'évaluation de votre travail est disponible dans votre espace personnel interne."
	echo
}

function message_3(){
	echo
	echo -e "$WHITE_B\bDe:$NORMAL Responsable communication réseaux"
	echo
	echo -e "$WHITE_B\bSujet:$NORMAL [Rappel] Mis en conformité des protocoles"
	echo
	echo -e "$WHITE_B\bDate:$NORMAL $JOUR 12:02:42"
	echo
	echo -e "$WHITE_B\bCorps:$NORMAL Les nouveaux protocoles de communication entreront en service dans 30 jours. Veuillez mettre en place les nouvelles fonctionnalités au sein des modules utilisateurs, relations, tiers."
	echo
}

function message_4(){
	echo
	echo -e "$WHITE_B\bDe:$NORMAL Service client"
	echo
	echo -e "$WHITE_B\bSujet:$NORMAL Ajout de la fonctionnalité #45"
	echo
	echo -e "$WHITE_B\bDate:$NORMAL $JOUR 09:00:03"
	echo
	echo -e "$WHITE_B\bCorps:$NORMAL La télé-réunion prévue demain portera sur les modalités d'implémentation de la fonctionnalité #45 : mise en cache des données des profils utilisateurs."
	echo
}

# ------ UTILS
function progress(){
	echo -n "."; sleep $SPEED;echo -n "."; sleep $SPEED;echo -n "."; sleep $SPEED;echo -n "."; sleep $SPEED;echo -n "."; sleep $SPEED;echo -n "."; sleep $SPEED;echo -n "."; sleep $SPEED;echo -n "."; sleep $SPEED;echo -n "."; sleep $SPEED;echo -n "."; sleep $SPEED;echo -n "."; sleep $SPEED;
}

function ligne() {
	echo -n -e "\t$BLACK_B\b[$1]$NORMAL $2 \n"
}


# ---- MENUS -----
function menu_help(){
	ligne "help" "Consultez l'aide"
	ligne "messages" "Messages reçus"
	ligne "taches" "Tâches en cours"
	ligne "quitter" "Quitter la session"
	ligne "terminal" "Ouvrir le terminal distant"
	#ligne "readme" "Éditer le fichier README.md"
	#ligne "budget" "Ouvrir le fichier classeur"
	#ligne "S" "Démarrer le serveur audio"
	#ligne "L" "Rédiger un texte de loi"
	#ligne "Q" "Répondre au questionnaire"
}

function menu_messages(){
	ligne "m1" " $RED_B\b[Urgent] Rapport de sécurité $ID$NORMAL"
	ligne "m2" "Rapport d'évaluation"
	ligne "m3" "Mis en conformité des protocoles"
	ligne "m4" "Ajout de la fonctionnalité #45"
}

function menu_tasks(){
	ligne "diagnostic" "Lancer l'outil de diagnostic"
	#ligne "resultats" "Résultats du diagnostic"
	ligne "nettoyer" "Nettoyer la base utilisateurs logWorld"
	ligne "evaluation" "Évaluation votre session de travail"
	ligne "verifier" "Vérifier les enregistrements"
	ligne "maj" "Mettre à jour le système"
	#ligne "valider" "Valider les décisions"
}

# --------------- DEBUT SCENARIO ------------------
# Sans argument = début du scénario
# Permet de revenir au menu sans passer par le debut
if [ $# -eq 0 ]
then
	debut_session
	SPEED=0.1
	progress
	echo -e $NORMAL
	run_sensors
	SPEED=0.2
	progress
	sleep 1
	clear
fi


# --------------- SCENARIO MENU ------------------
# Invite de commandes
# Boucle infinie qui s'arrête avec break
while [ 1 ]; do
	# Login
	echo -n -e "$BOLD$LOGIN@logarien$NORMAL $ "
	read reps

	# Commandes
	case $reps in
		capteurs | sensors)
			curl http://localhost:3000/sensors  2>&1 &
			;;
		taches | tasks | todo )
		   echo -e "Que souhaitez-vous faire aujourd'hui ?"
		   menu_tasks
		   ;;
	  formes | shapes | forme)
			curl http://localhost:3000/shapes  2>&1 &
			;;
		help )
		   menu_help
		   ;;
		evaluation )
			echo -e "Envoie de la requête au serveur d'évaluation"
			sleep 2
			curl http://localhost:3000/questionnaire 2>&1 &
			 #killall firefox
		   #$SCRIPT_FORM &
		   ;;
		messages )
		   menu_messages
		   ;;
		m1 )
		   message_1
		   ;;
		m2 )
		   message_2
		   ;;
		m3 )
		   message_3
		   ;;
		m4 )
		   message_4
		   ;;
		nettoyer )
		   echo -e -n "Êtes-vous certain de vouloir poursuivre ? [oui] : "
		   read YES
		   case $YES in
		   	o | Y | yes | oui )
		   		echo -e "$WHITE_B\b[Info] Passage en mode administrateur$NORMAL"
		   		echo
		   		echo -e "sudo clean --force logworld"
		   		echo
		   		SPEED=0.5
		   		progress
		   		SPEED=0.3
		   		progress
		   		SPEED=0.1
		   		progress
		   		echo
		   		#cp "$DIR/message.txt" .
					echo -e -n "Message : "
					read MSG
		   		echo
		   		echo -e -n "Envoyer ? [oui] : "
		   		read ENVOI_MSG
		   		SPEED=0.3
		   		progress
		   		echo
		   		;;
		   	n | N | no | non)
		   	  echo -e "$RED_B[Erreur : 101.B]$NORMAL"
		   		echo -e "Nous avons détecté une défaillance d'éxecution."
		   		echo -e "Envoyer le rapport de logs à l'administration."
		   		;;
		   esac
		   ;;
		maj )
		   echo -e "Mise à jour déjà effectuée."
		   ;;
		valider )
		   echo -e "Décisions reportées. Se référer au groupe de discussions de l'agenda."
		   ;;
		verifier | check )
		   echo -e "Vous n'avez pas les droits. Contactez le service."
		   ;;
		readme )
		   vim README.md &
		   ;;
		budget )
		   echo -e "Vous n'avez pas les droits. Contactez le service comptabilité."
		   ;;
		terminal )
		  echo -e "Vous n'avez pas les droits. Contactez le service système."
		  ;;
		diagnostic )
			 #curl http://localhost:3000/shapes 2>&1 &
		   $SCRIPT_PD &
			 #affiche_resultats
		   #curl http://localhost:3000/pd 2>&1 &
		   ;;
		loi )
			echo -e "Vous n'avez pas les droits. Contactez le service associé à la rédaction des cadres juridiques du groupe."
		  ;;
		about | --v  )
		   echo -e "Programme de connection aux logs."
		   ;;
		clear | vider | vide )
		   clear
			 killall curl
		   ;;
		quit | exit | quitter | laisser | sortir | "s'enfuir")
		   echo -e "Déconnexion du serveur."
		   break;;
		resultats )
		   affiche_resultats
		   ;;
		repondre )
		   echo -e -n "Réponse à envoyer : "
		   read ENVOI_MSG
		   SPEED=0.1
		   progress
		   echo
		   ;;
		* )
		  echo -e "Commande inconnue..."
		  echo -e "Tapez help pour afficher la liste des commandes."
		  ;;
	esac
done
