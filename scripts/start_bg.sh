#! /bin/bash
# Start background processus

killall xinput 2>&1
killall python3 2>&1
killall node 2>&1
killall npm 2>&1

# Get absolute path of this script
DIR="$(cd "$(dirname "$0")" && pwd)"

# Launch node
cd "$DIR/../node"
/usr/bin/node "$DIR/../node/src/app.js" &

# Heart sensor (Pd patch)
#sleep 1
#PD_HEART="$DIR/../pd/_heart.pd"
#pd -nogui -noaudio -nomidi -open $PD_HEART 2>&1 &

sleep 2


# Patch names
PD_MAIN="$DIR/../pd/main.pd"
PD_BLANK="$DIR/../pd/_blank.pd"

# Clear main patch
rm $PD_MAIN
cp $PD_BLANK $PD_MAIN


# Webcam settings
# v4l2-ctl --list-devices
# v4l2-ctl --list-ctrls
# Kurokesu C1 PRO SN000021: Kurok (usb-0000:00:14.0-2): /dev/video0, /dev/video1, /dev/video2, /dev/video3
# Laptop_Integrated_Webcam_HD: In (usb-0000:00:1a.0-1.5): /dev/video4, /dev/video5
# v4l2-ctl --set-ctrl=zoom_absolute=120 -d /dev/video2
# v4l2-ctl --set-ctrl=focus_auto=0 -d /dev/video2
# sleep 1

#xinput list change avec Webcam !!
#KEYBOARD=13
KEYBOARD=12
if [ $# -gt 0 ]
then
	KEYBOARD=$1
fi


echo $KEYBOARD

# Keylogger python
xinput --test $KEYBOARD | python3 $DIR/keyOSC.py 2>&1 &
