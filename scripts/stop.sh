#! /bin/bash

# Kill all processus
killall pd 2>&1
killall xinput 2>&1
killall python3 2>&1
killall /snap/chromium/733/usr/lib/chromium-browser/chrome 2>&1
killall node 2>&1
killall npm 2>&1